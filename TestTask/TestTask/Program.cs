﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1. Constuctor
            DateTime dt1 = new DateTime(2008, 1, 9);
            Quarter q1 = new Quarter(dt1);
            Console.WriteLine("1: OK");

            // 2. Equality-related methods
            Console.WriteLine("2:");
            q1 = new Quarter(new DateTime(2017, 1, 1));
            Quarter q1a = new Quarter(new DateTime(2017, 2, 1));
            Quarter q2 = new Quarter(new DateTime(2017, 4, 1));
            if (q1 == q1a)
            {
                Console.WriteLine("true");
            } // true
            else
            {
                Console.WriteLine("Error!");
            }

            if (q1 == q2)
            {
                Console.WriteLine("Error!");
            } // false
            else
            {
                Console.WriteLine("false");
            }

            if (Object.Equals(q1, q1a))
            {
                Console.WriteLine("true");
            } // true
            else
            {
                Console.WriteLine("Error!");
            }

            var dict = new Dictionary<Quarter, decimal>();

            dict.Add(q1, 100m);

            if (dict.ContainsKey(q1a))
            {
                Console.WriteLine("true");
            } // true
            else
            {
                Console.WriteLine("Error!");
            }

            // 3. Comparison-related methods
            Console.WriteLine("3:");
            if (q1 > q1a)
            {
                Console.WriteLine("Error!");
            } // false
            else
            {
                Console.WriteLine("false");
            }

            if (q2 > q1)
            {
                Console.WriteLine("true");
            } 
            else
            {
                Console.WriteLine("Error!");
            }// true

            var list = new List<Quarter>() { q2, q1 };
            list.Sort(); // after that items in the list must be in the following order: q1, q2
            if ((list[0] == q1) && (list[1] == q2))
            {
                Console.WriteLine("true");
            }
            else
            {
                Console.WriteLine("Error!");
            }

            // 4. All required code to make this statement compile & work: Quarter q = DateTime.UtcNow;
            Quarter q = DateTime.UtcNow;
            Console.WriteLine("4: OK");

            // 5. IFormattable.ToString function
            Console.WriteLine("5:");
            Console.WriteLine("y: 2007-06-15 -> " + 
                new Quarter(new DateTime(2007, 6, 15)).ToString("y"));
            Console.WriteLine("yy: 2019-06-15 -> " + 
                new Quarter(new DateTime(2019, 6, 15)).ToString("yy"));
            Console.WriteLine("yyy: 0001-01-01 -> " + 
                new Quarter(new DateTime(0001, 1, 1)).ToString("yyy"));
            Console.WriteLine("yyyy: 2009-06-15 -> " + 
                new Quarter(new DateTime(2009, 6, 15)).ToString("yyyy"));
            Console.WriteLine("yyyyy: 2009-04-21 -> " + 
                new Quarter(new DateTime(2009, 4, 21)).ToString("yyyyy"));
            Console.WriteLine("Q: 2009-04-21 -> " + 
                new Quarter(new DateTime(2009, 4, 21)).ToString("Q"));
            Console.WriteLine("\\: 2009-04-21 (\\QQ) -> " +
                new Quarter(new DateTime(2009, 4, 21)).ToString("\\QQ"));
            Console.WriteLine("\'string\': 2009-04-21 (\'Quarter\' Q) -> " + 
                new Quarter(new DateTime(2009, 4, 21)).ToString("\'Quarter\' Q"));
            Console.WriteLine("\"string\": 2009-04-21 (\"Quarter\" Q) -> " +
                new Quarter(new DateTime(2009, 4, 21)).ToString("\"Quarter\" Q"));
            Console.WriteLine("Any other character: 2009-04-21 (in yyyy) -> " + 
                new Quarter(new DateTime(2009, 4, 21)).ToString("in yyyy"));

            string str;
            Console.WriteLine();
            Console.WriteLine("Input format for data " + dt1 + " or \'e\' to exit");
            str = Console.ReadLine();
            while (str != "e")
            {
                Console.WriteLine(str + " -> " + q1.ToString(str));
                Console.WriteLine("Input format for data " + dt1 + " or \'e\' to exit");
                str = Console.ReadLine();
            }
        }
    }
}
