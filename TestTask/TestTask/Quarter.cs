﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask
{
    class Quarter : IFormattable, IComparable<Quarter>
    {
        public Quarter(DateTime dt)
        {
            dateTime = dt;
        }

        public static bool operator ==(Quarter quarter1, Quarter quarter2)
        {
            return ((quarter1.GetQuarter() == quarter2.GetQuarter()) &&
                    (quarter1.dateTime.Year == quarter2.dateTime.Year));
        }

        public static bool operator !=(Quarter quarter1, Quarter quarter2)
        {
            return ((quarter1.GetQuarter() != quarter2.GetQuarter()) ||
                    (quarter1.dateTime.Year != quarter2.dateTime.Year));
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Quarter))
                return false;

            Quarter quarter = (Quarter) obj;
            return ((GetQuarter() == quarter.GetQuarter()) &&
                    (dateTime.Year == quarter.dateTime.Year));
        }

        public override int GetHashCode()
        {
            return (GetQuarter() * dateTime.Year).GetHashCode();
        }

        public static bool operator >(Quarter quarter1, Quarter quarter2)
        {
            return ((quarter1.dateTime.Year > quarter2.dateTime.Year) ||
                    ((quarter1.dateTime.Year == quarter1.dateTime.Year) && (quarter1.GetQuarter() > quarter2.GetQuarter())));
        }

        public static bool operator <(Quarter quarter1, Quarter quarter2)
        {
            return ((quarter1.dateTime.Year < quarter2.dateTime.Year) ||
                    ((quarter1.dateTime.Year == quarter1.dateTime.Year) && (quarter1.GetQuarter() < quarter2.GetQuarter())));
        }

        public static bool operator >=(Quarter quarter1, Quarter quarter2)
        {
            return ((quarter1.dateTime.Year > quarter2.dateTime.Year) ||
                    ((quarter1.dateTime.Year == quarter1.dateTime.Year) && (quarter1.GetQuarter() >= quarter2.GetQuarter())));
        }

        public static bool operator <=(Quarter quarter1, Quarter quarter2)
        {
            return ((quarter1.dateTime.Year < quarter2.dateTime.Year) ||
                    ((quarter1.dateTime.Year == quarter1.dateTime.Year) && (quarter1.GetQuarter() <= quarter2.GetQuarter())));
        }

        public int CompareTo(Quarter quarter)
        {
            if (this > quarter)
            {
                return 1;
            }
            else
                if (this < quarter)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
        }

        public string ToString(string format)
        {
            return ToString(format, CultureInfo.CurrentCulture);
        }

        private bool IsFormat1(string format)
        {
            return ((format.Length > 0) && (format[0] == 'y') && (!IsFormat2(format)) &&
                (!IsFormat3(format)) && (!IsFormat4(format)) && (!IsFormat5(format))) ;
        }

        private bool IsFormat2(string format)
        {
            return ((format.Length > 1) && (format.StartsWith("yy")) && (!IsFormat3(format)) &&
                (!IsFormat4(format)) && (!IsFormat5(format)));
        }

        private bool IsFormat3(string format)
        {
            return ((format.Length > 2) && (format.StartsWith("yyy")) && (!IsFormat4(format)) &&
                (!IsFormat5(format)));
        }

        private bool IsFormat4(string format)
        {
            return ((format.Length > 3) && (format.StartsWith("yyyy")) && (!IsFormat5(format)));
        }

        private bool IsFormat5(string format)
        {
            return ((format.Length > 4) && (format.StartsWith("yyyyy")));
        }

        private bool IsFormat6(string format)
        {
            return ((format.Length > 0) && (format[0] == 'Q'));
        }

        private bool IsFormat7(string format)
        {
            return ((format.Length > 0) && (format[0] == '\\'));
        }

        private bool IsFormat8_1(string format)
        {
            return ((format.Length > 0) && (format[0] == '\"'));
        }

        private bool IsFormat8_2(string format)
        {
            return ((format.Length > 0) && (format[0] == '\''));
        }

        public string ToString(string format, IFormatProvider provider)
        {
            string result = "";

            if (format == "")
                return result;

            while (format[0] == ' ')
            {
                result += " ";
                format = format.Remove(0, 1);
            }

            if (format == "")
                return result;

            if (IsFormat1(format))
            {
                result += (dateTime.Year%100).ToString();
                format = format.Remove(0, 1);
                return result + ToString(format, provider);
            }

            if (IsFormat2(format))
            {
                result += dateTime.ToString("yy");
                format = format.Remove(0, 2);
                return result + ToString(format, provider);
            }

            if (IsFormat3(format))
            {
                result += dateTime.ToString("yyy");
                format = format.Remove(0, 3);
                return result + ToString(format, provider);
            }

            if (IsFormat4(format))
            {
                result += dateTime.ToString("yyyy");
                format = format.Remove(0, 4);
                return result + ToString(format, provider);
            }

            if (IsFormat5(format))
            {
                result += dateTime.ToString("yyyyy");
                format = format.Remove(0, 5);
                return result + ToString(format, provider);
            }

            if (IsFormat6(format))
            {
                result += GetQuarter().ToString();
                format = format.Remove(0, 1);
                return result + ToString(format, provider);
            }

            if (IsFormat7(format))
            {
                format = format.Remove(0, 1);
                if (format.Length > 0)
                {
                    result += format[0].ToString();
                    format = format.Remove(0, 1);
                    return result + ToString(format, provider);
                }
                else
                    return result;
            }

            if (IsFormat8_1(format))
            {
                result += format[0].ToString();
                format = format.Remove(0, 1);
                int pos = format.IndexOf('\"');
                if (pos >= 0)
                {
                    result += format.Substring(0, pos + 1);
                    format = format.Remove(0, pos + 1);
                }
                return result + ToString(format);
            }

            if (IsFormat8_2(format))
            {
                result += format[0].ToString();
                format = format.Remove(0, 1);
                int pos = format.IndexOf('\'');
                if (pos >= 0)
                {
                    result += format.Substring(0, pos + 1);
                    format = format.Remove(0, pos + 1);
                }
                return result + ToString(format);
            }

            result += format[0].ToString();
            format = format.Remove(0, 1);
            return result + ToString(format);
        }

        public static implicit operator Quarter(DateTime dt)
        {
            return new Quarter(dt);
        }

        private int GetQuarter()
        {
            return (int)Math.Ceiling(dateTime.Month/3.0);
        }

        private DateTime dateTime { set; get; }
    }
}
